//
//  RPApp.swift
//  Radioplayer
//
//  Created by Roman Putintsev on 24.03.2021.
//

import SwiftUI
import MediaPlayer

@main
struct RPApp: App {
    let radioModel = RPRadioModel()

    init() {
        // Get the shared MPRemoteCommandCenter
        let commandCenter = MPRemoteCommandCenter.shared()

        commandCenter.playCommand.isEnabled = true
        commandCenter.playCommand.addTarget { [self] event in
            self.radioModel.play()
            return .success
        }

        commandCenter.pauseCommand.isEnabled = true
        commandCenter.pauseCommand.addTarget { [self] event in
            self.radioModel.stop()
            return .success
        }
    }

    var body: some Scene {
        WindowGroup {
            GeometryReader { geometry in
                ZStack {
                    VStack(alignment: .leading, spacing: 0) {
                        RPPallete.mainBackground
                        RPPallete.panelBackground.frame(height: geometry.safeAreaInsets.bottom+8)
                    }.ignoresSafeArea()
                    RPMainView().environmentObject(radioModel)
                }
            }
        }
    }
}
