//
//  RPMainView.swift
//  Radioplayer
//
//  Created by Roman Putintsev on 24.03.2021.
//

import SwiftUI
import SwURL

struct RPMainView: View {
    @EnvironmentObject var radioModel: RPRadioModel

    var body: some View {
        VStack(spacing: 0) {
            ScrollView {
                LazyVStack(spacing: 0) {
                    ForEach(radioModel.stations.indices, id: \.self) { i in
                        RPStationRow(station: radioModel.stations[i], stationIndex: i)
                    }
                }
            }.padding(.top, 10)

            ZStack {
                RoundedRectangle(cornerRadius: 8)
                    .fill(RPPallete.panelBackground)
                HStack(spacing: 0) {
                    ZStack {
                        Image("cover")
                            .resizable()
                            .aspectRatio(contentMode: .fill)
                        RemoteImageView(
                            url: (URL(string: radioModel.metaCoverUrl) ?? URL(string: "cover"))!
                        ).imageProcessing({ image in
                            return image
                                .resizable()
                                .aspectRatio(contentMode: .fill)
                        }).opacity(radioModel.metaCoverUrl.isEmpty ? 0 : 1)
                    }.clipShape(RoundedRectangle(cornerRadius: 3))
                    .frame(width: 48, height: 48)
                    .padding(.all, 16)

                    VStack {
                        Text(radioModel.currentStation.frequency < 1
                                ? radioModel.currentStation.name
                                : String(format: "%.1f FM \(radioModel.currentStation.name)", radioModel.currentStation.frequency))
                            .font(.custom("RobotoCondensed-Regular", size: 14))
                            .frame(maxWidth: .infinity, alignment: .leading)
                            .foregroundColor(RPPallete.panelText)
                            .textCase(.uppercase)
                        Spacer()
                        Text(radioModel.meta)
                            .font(.custom("Roboto-Regular", size: 14))
                            .frame(maxWidth: .infinity, alignment: .leading)
                            .foregroundColor(RPPallete.panelText)
                            .opacity(0.3)
                    }.padding(.vertical, 19)

                    RPPlayButton()
                }
            }.frame(maxHeight: 80)
        }
    }
}

struct RPMainView_Previews: PreviewProvider {
    static var previews: some View {
        RPMainView()
    }
}
