//
//  RPPallete.swift
//  Radioplayer
//
//  Created by Roman Putintsev on 02.04.2021.
//

import SwiftUI

struct RPPallete {
    static let mainActive = Color("mainActive")
    static let mainBackground = Color("mainBackground")
    static let mainText = Color("mainText")
    static let panelBackground = Color("panelBackground")
    static let panelText = Color("panelText")
}
