//
//  RPPlayButton.swift
//  Radioplayer
//
//  Created by Roman Putintsev on 02.04.2021.
//

import SwiftUI

struct RPPlayButton: View {
    @State private var stalledSpin = false
    @EnvironmentObject var radioModel: RPRadioModel

    var body: some View {
        Button(action: {
            if (radioModel.isPlaying) {
                radioModel.stop()
            } else {
                radioModel.play()
            }
        }) {
            ZStack {
                Circle()
                    .stroke(Color.white, lineWidth: 1)
                    .opacity(0.2)
                Image("iconPlay")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .opacity(radioModel.isPlaying ? 0 : 1)
                Image("iconStop")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .opacity(radioModel.isPlaying ? 1 : 0)
                Circle()
                    .trim(from: 0, to: 0.2)
                    .stroke(Color.white, lineWidth: 1)
                    .rotationEffect(Angle(degrees: stalledSpin ? 360: 0))
                    .animation(Animation.linear(duration: 1.5).repeatForever(autoreverses: false))
                    .opacity(radioModel.isStalled ? 1 : 0)
                    .onAppear() {
                        self.stalledSpin.toggle()
                    }
            }.frame(width: 52, height: 52)
            .padding(.all, 14)
        }
    }
}

struct RPPlayButton_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            RPPlayButton()
        }
    }
}
