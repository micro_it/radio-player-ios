//
//  RPStationRow.swift
//  Radioplayer
//
//  Created by Roman Putintsev on 05.04.2021.
//

import SwiftUI

struct RPStationRow: View {
    @EnvironmentObject var radioModel: RPRadioModel
    var station: RPRadioPlayerStation
    var stationIndex: Int

    var body: some View {
        Button(action: {
            radioModel.currentStationIndex = stationIndex
            if (!radioModel.isPlaying) {
                radioModel.play()
            }
        }) {
            HStack(spacing: 20) {
                ZStack {
                    RoundedRectangle(cornerRadius: 32)
                        .fill(stationIndex == radioModel.currentStationIndex ? Color.white : RPPallete.mainText)
                        .opacity(stationIndex == radioModel.currentStationIndex ? 1 : 0.07)
                    Rectangle()
                        .fill(RPPallete.mainText)
                        .mask(Image("logoBlack")
                                .resizable()
                                .aspectRatio(contentMode: .fit))
                        .opacity(stationIndex == radioModel.currentStationIndex ? 0 : 0.5)
                    Image("logoBlack")
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .opacity(stationIndex == radioModel.currentStationIndex ? 1 : 0)
                }.frame(width: 64, height: 64)
                .opacity(station.enabled ? 1 : 0.05)

                VStack {
                    Text(station.name)
                        .font(.custom("RobotoCondensed-Regular", size: 18))
                        .frame(maxWidth: .infinity, alignment: .leading)
                        .textCase(.uppercase)
                        .foregroundColor(stationIndex == radioModel.currentStationIndex ? RPPallete.mainActive : RPPallete.mainText)
                        .opacity(station.enabled ? 1 : 0.2)
                    Spacer()
                    Text(String(format: "%.1f FM", station.frequency))
                        .font(.custom("Roboto-Regular", size: 14))
                        .frame(maxWidth: .infinity, alignment: .leading)
                        .textCase(.uppercase)
                        .foregroundColor(RPPallete.mainText)
                        .opacity(station.frequency > 1 ? (station.enabled ? 0.3 : 0.2) : 0)
                }.frame(height: 44)
            }.frame(height: 84)
            .padding(.horizontal, 20)
        }
    }
}
