//
//  RPRadioModel.swift
//  Radioplayer
//
//  Created by Roman Putintsev on 02.04.2021.
//

import Foundation
import MediaPlayer

final class RPRadioModel: ObservableObject {
    @Published fileprivate(set) var stations: [RPRadioPlayerStation] = []
    @Published fileprivate(set) var isPlaying: Bool = false
    @Published fileprivate(set) var isStalled: Bool = false
    @Published fileprivate(set) var meta: String = "" {
        didSet {
            nowPlayingInfo[MPMediaItemPropertyArtist] = meta as AnyObject
            MPNowPlayingInfoCenter.default().nowPlayingInfo = nowPlayingInfo
        }
    }
    @Published fileprivate(set) var metaCoverUrl: String = ""
    @Published fileprivate(set) var currentStation: RPRadioPlayerStation {
        didSet {
            player?.setStreamUrl(currentStation.mobileSourceUrl)
            metaUpdater?.setMetaUrl(currentStation.metadataJsonUrl)
            metaUpdater?.setMetaIdUrl(currentStation.metadataIdJsonUrl)

            nowPlayingInfo[MPMediaItemPropertyTitle] = (currentStation.name ?? "") as AnyObject
            MPNowPlayingInfoCenter.default().nowPlayingInfo = nowPlayingInfo
        }
    }
    @Published var currentStationIndex: Int = -1 {
        didSet {
            if (currentStationIndex < 0 || currentStationIndex >= stations.count) {
                if currentStationIndex != -1 {
                    currentStationIndex = -1
                }
                currentStation = RPRadioPlayerStation()
            } else {
                currentStation = stations[currentStationIndex]
            }
        }
    }

    fileprivate let CURRENT_REGION_ID: Int32 = 40

    fileprivate var player: STStartrekPlayer? = nil
    fileprivate var metaUpdater: STStartrekMetadataUpdater? = nil
    fileprivate var api: RPRadioPlayerApi? = nil
    var nowPlayingInfo: [String: AnyObject] = [ : ];

    init() {
        currentStation = RPRadioPlayerStation()

        player = STStartrekPlayer.create()
        player?.setDelegate(RPPlayerDelegate(parent: self))

        metaUpdater = STStartrekMetadataUpdater.create()
        metaUpdater?.setDelegate(RPMetaUpdaterDelegate(parent: self))

        api = RPRadioPlayerApi.create()
        api?.setDelegate(RPApiDelegate(parent: self))

        api?.getStations(CURRENT_REGION_ID)

        nowPlayingInfo[MPMediaItemPropertyTitle] = (currentStation.name ?? "") as AnyObject
        nowPlayingInfo[MPMediaItemPropertyArtist] = meta as AnyObject
        nowPlayingInfo[MPNowPlayingInfoPropertyIsLiveStream] = true as AnyObject
        MPNowPlayingInfoCenter.default().playbackState = self.nowPlayingState()
        MPNowPlayingInfoCenter.default().nowPlayingInfo = nowPlayingInfo
    }

    func nowPlayingState() -> MPNowPlayingPlaybackState {
        switch player?.state() {
            case .none:
                return MPNowPlayingPlaybackState.unknown
            case .some(STStartrekPlayerState.PLAYINGSTATE):
                return MPNowPlayingPlaybackState.playing
            case .some(.STALLEDSTATE):
                return MPNowPlayingPlaybackState.interrupted
            case .some(.PAUSEDSTATE):
                return MPNowPlayingPlaybackState.paused
            case .some(.STOPPEDSTATE):
                return MPNowPlayingPlaybackState.stopped
            case .some(.ERRORSTATE):
                return MPNowPlayingPlaybackState.unknown
            case .some(_):
                return MPNowPlayingPlaybackState.unknown
        }
    }

    func play() {
        if currentStationIndex < 0 && stations.count > 0 {
            currentStationIndex = 0
        }
        player?.play()
    }

    func stop() {
        player?.stop()
    }

    fileprivate func updateMeta() {
        let title: String = metaUpdater?.title() ?? ""
        let subtitle: String = metaUpdater?.subtitle() ?? ""
        var newMeta = title
        if newMeta.isEmpty {
            newMeta = subtitle
        } else {
            newMeta = title + " - " + subtitle
        }
        meta = newMeta
    }
}

final class RPPlayerDelegate: STStartrekPlayerDelegate {
    private var p: RPRadioModel

    init(parent: RPRadioModel) {
        self.p = parent
        self.p.isPlaying = (self.p.player?.isPlaying() ?? false)
        self.p.isStalled = (self.p.player?.isStalled() ?? false)
    }

    func ended() {}
    func error(_ message: String) {}
    func streamUrlChanged(_ streamUrl: String) {}
    func isRestartedChanged(_ isRestarted: Bool) {}
    func isHlsChanged(_ isHls: Bool) {}
    func isSeekableChanged(_ isSeekable: Bool) {}
    func stateChanged(_ state: STStartrekPlayerState) {
        DispatchQueue.main.async {
            MPNowPlayingInfoCenter.default().playbackState = self.p.nowPlayingState()
        }
    }

    func isPlayingChanged(_ isPlaying: Bool) {
        DispatchQueue.main.async {
            self.p.isPlaying = isPlaying
        }
    }

    func isStalledChanged(_ isStalled: Bool) {
        DispatchQueue.main.async {
            self.p.isStalled = isStalled
        }
    }

    func isPausedChanged(_ isPaused: Bool) {}
    func isStoppedChanged(_ isStopped: Bool) {}
    func lengthChanged(_ length: Double) {}

    func bufferedLengthChanged(_ bufferedLength: Double) {}
    func startPositionChanged(_ startPosition: Double) {}
    func positionChanged(_ position: Double) {}
    func playbackRateChanged(_ playbackRate: Double) {}
    func metaChanged(_ meta: String) {
        DispatchQueue.main.async {
            self.p.updateMeta()
        }
    }
    func volumeChanged(_ volume: Double) {}
    func duckVolumeChanged(_ duckVolume: Bool) {}
    func playingBitratesChanged(_ playingBitrates: [NSNumber]) {}
    func availableBitratesChanged(_ availableBitrates: [NSNumber]) {}
    func currentBitrateChanged(_ currentBitrate: Int32) {}
    func currentQualityChanged(_ currentQuality: STStartrekPlayerQuality) {}
    func playingBitrateChanged(_ playingBitrate: Int32) {}
    func playingQualityChanged(_ playingQuality: STStartrekPlayerQuality) {}
    func daastUrlChanged(_ daastUrl: String) {}
    func daastStarted(_ meta: String, imageUrl: String, clickUrl: String) {}
    func daastError(_ message: String) {}
    func daastSkipped() {}
    func daastEnded() {}
}

final class RPMetaUpdaterDelegate : STStartrekMetadataUpdaterDelegate {
    private var p: RPRadioModel

    init(parent: RPRadioModel) {
        self.p = parent
    }

    func metaUrlChanged(_ metaUrl: String) {}
    func metaIdUrlChanged(_ metaIdUrl: String) {}
    func titleChanged(_ title: String) {
        DispatchQueue.main.async {
            self.p.updateMeta()
        }
    }
    func subtitleChanged(_ subtitle: String) {
        DispatchQueue.main.async {
            self.p.updateMeta()
        }
    }
    func coverUrlChanged(_ coverUrl: String) {
        DispatchQueue.main.async {
            self.p.metaCoverUrl = coverUrl
        }
    }
    func ddbIdChanged(_ ddbId: Int32) {}
    func typeChanged(_ type: Int32) {}
}

final class RPApiDelegate : RPRadioPlayerApiDelegate {
    private var p: RPRadioModel

    init(parent: RPRadioModel) {
        self.p = parent
    }

    func regionResult(_ region: RPRadioPlayerRegion, error: String) {}
    func regionsResult(_ regions: [RPRadioPlayerRegion], error: String) {}
    func stationsResult(_ stations: [RPRadioPlayerStation], error: String) {
        DispatchQueue.main.async {
            if !error.isEmpty {
                print("Stations update error \(error)")
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                    self.p.api?.getStations(self.p.CURRENT_REGION_ID)
                }
            } else {
                self.p.stations = stations
            }
        }
    }
    func optionsResult(_ googleAnalytics: String, yandexMetrica: String, lastModifyTs: Int64, error: String) {}
    func messageResult(_ error: String) {}
    func favoriteResult(_ error: String) {}
}
