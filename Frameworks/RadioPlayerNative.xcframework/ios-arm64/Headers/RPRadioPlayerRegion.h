// AUTOGENERATED FILE - DO NOT MODIFY!
// This file generated by Djinni from RadioPlayer.djinni

#import <Foundation/Foundation.h>

@interface RPRadioPlayerRegion : NSObject
- (nonnull instancetype)initWithId:(int32_t)id
                              name:(nonnull NSString *)name
                              code:(nonnull NSString *)code;
+ (nonnull instancetype)RadioPlayerRegionWithId:(int32_t)id
                                           name:(nonnull NSString *)name
                                           code:(nonnull NSString *)code;

@property (nonatomic, readonly) int32_t id;

@property (nonatomic, readonly, nonnull) NSString * name;

@property (nonatomic, readonly, nonnull) NSString * code;

@end
